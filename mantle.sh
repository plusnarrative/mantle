#!/usr/bin/env bash

sudo apt-add-repository ppa:ondrej/php

sudo apt-get update

sudo apt-get install -y nginx
sudo apt-get install -y php7.0 php7.0-fpm

sudo debconf-set-selections <<< 'mysql-server-5.5 mysql-server/root_password password mantle'
sudo debconf-set-selections <<< 'mysql-server-5.5 mysql-server/root_password_again password mantle'
sudo apt-get -y install mysql-server-5.5 php7.0-mysql

# Build database
if [ ! -f /var/log/databasesetup ];
then
    echo "CREATE USER 'mantle'@'localhost' IDENTIFIED BY 'mantle'" | mysql -uroot -pmantle
    echo "CREATE DATABASE mantle" | mysql -uroot -pmantle
    echo "GRANT ALL ON mantle.* TO 'mantle'@'localhost'" | mysql -uroot -pmantle
    echo "flush privileges" | mysql -uroot -pmantle

    touch /var/log/databasesetup

    if [ -f /vagrant/data/initial.sql ];
    then
        mysql -uroot -pmantle mantle < /vagrant/data/initial.sql
    fi
fi

# Link virtual drive to host
if ! [ -L /var/www ]; then
  rm -rf /var/www
  ln -fs /vagrant /var/www
fi

# Configure nginx
sudo cp /vagrant/default /etc/nginx/sites-available/

# Restart services
sudo service nginx restart
