![Screen Shot 2016-02-14 at 1.59.26 PM.png](https://bitbucket.org/repo/aqArEE/images/4067602099-Screen%20Shot%202016-02-14%20at%201.59.26%20PM.png)

**The fastest way to spin up a virtual Ubuntu server with Nginx, PHP7 and MySQL.**

Written by: Lliam Scholtz (@lliamscholtz) | Version: 1.0

### Requirements: ###

1. VirtualBox (https://www.virtualbox.org/)
2. Vagrant (https://www.vagrantup.com/downloads.html)

### Setup: ###
 
```
#!shell

git clone https://plusnarrative@bitbucket.org/plusnarrative/mantle.git
```
```
#!shell

bash install.sh
```

### Launch: ###

```
#!shell

bash launch.sh
```

### Versions: ###

* Ubuntu 14.04.3 LTS (Trusty Tahr) - 64bit
* Nginx/1.4.6 (Ubuntu)
* PHP 7.0.3-3+deb.sury.org~trusty+1 (cli) (NTS)
* MySQL (coming soon)