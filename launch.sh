#
# My first shell script
#
clear
echo '------------------------------------------------------'
echo ''
echo '███╗   ███╗ █████╗ ███╗   ██╗████████╗██╗     ███████╗'
echo '████╗ ████║██╔══██╗████╗  ██║╚══██╔══╝██║     ██╔════╝'
echo '██╔████╔██║███████║██╔██╗ ██║   ██║   ██║     █████╗  '
echo '██║╚██╔╝██║██╔══██║██║╚██╗██║   ██║   ██║     ██╔══╝  '
echo '██║ ╚═╝ ██║██║  ██║██║ ╚████║   ██║   ███████╗███████╗'
echo '╚═╝     ╚═╝╚═╝  ╚═╝╚═╝  ╚═══╝   ╚═╝   ╚══════╝╚══════╝'
echo ''
echo 'The fastest way to spin up a virtual Ubuntu server with'
echo 'Nginx, PHP7 and MySQL.'
echo ''
echo 'Written by: Lliam Scholtz (@lliamscholtz)'
echo 'Version: 1.0'
echo ''
echo '------------------------------------------------------'
echo ''
echo "[*] Launching box ..."
vagrant up
echo "[*] SSHing into box ..."
vagrant ssh
